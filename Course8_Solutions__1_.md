<a href="https://www.bigdatauniversity.com"><img src="https://ibm.box.com/shared/static/cw2c7r3o20w9zn8gkecaeyjhgw3xdgbj.png" width="400" align="center"></a>

<h1 align="center"><font size="5">Classification with Python</font></h1>

<h2>Table of Contents</h2>
<div class="alert alert-block alert-info" style="margin-top: 20px">
    <ul>
        <li><a href="#Section_1"> Importing Data </a></li>
    <li><a href="#Section_2">Data Preprocessing</a> </li>
    <li><a href="#Section_3">One Hot Encoding </a></li>
    <li><a href="#Section_4">Training Data and Test Data </a></li>
    <li><a href="#Section_5">Instructions</a></li>
    <li><a href="#Section_6">Q1) Train a logistic regression model, identify best value of C parameter using GridSearchCV. Determine the accuracy of the test data. Also return Jaccard Index, log_loss and F1 Score on the test data
</a></li>
    <li><a href="#Section_7">Q2) Train a KNN model, find optimal values for the parameters : n_neighbors = [1,3,5,7,9], algorithm, and p, using GridSearchCV. Determine the accuracy of the test data . Also return Jaccard Index and F1 Score on the test data  </a></li>
    <li><a href="#Section_8">Q3) Train a SVM model,  find optimal values for parameters such as C = [.001, .01, .1, 1, 10, 100] and kernel=['linear', 'poly', 'rbf', 'sigmoid'],using GridSearchCV. Determine the accuracy of the test data . Also return Jaccard Index and F1 Score on the test data</a></li>
    <li><a href="#Section_9"> Q4) Train a Decision Tree, find optimal values for parameters such as criterion=['gini', 'entropy'], using GridSearchCV. Determine the accuracy of the test data . Also return Jaccard Index and F1 Score on the test data</a></li>
    <li><a href="#Section_10">Q5) Show the Accuracy,Jaccard,F1-Score and Log Loss in a tabular format using data frame for all of the above model</a></li>
<p>Estimated Time Needed: <strong>180 min</strong></p>
</div>

<hr>

In this notebook we try to practice all the classification algorithms that we learned in this course.

We load a dataset using Pandas library, and apply the following algorithms, and find the best one for this specific dataset by accuracy evaluation methods.

Lets first load required libraries:


```python
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn import svm
import types
from botocore.client import Config
import ibm_boto3
from sklearn.metrics import jaccard_similarity_score
from sklearn.metrics import f1_score
from sklearn.metrics import log_loss
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.naive_bayes import GaussianNB
```

Since sklearn calculates jaccard index differently than what was taught in the course we will define our own function for jaccard index


```python
def jaccard_index(predictions, true):
    if (len(predictions) == len(true)):
        intersect = 0;
        for x,y in zip(predictions, true):
            if (x == y):
                intersect += 1
        return intersect / (len(predictions) + len(true) - intersect)
    else:
        return -1
```

# Data

## About the Data

The original source of the data is Austrainlin Government's Bureau of Meterology and the latest data can be gathered from http://www.bom.gov.au/climate/dwo/.

The dataset we will use has extra columns like RainToday and our target RainTomorrow which was gathered from Rattle at https://bitbucket.org/kayontoga/rattle/src/master/data/weatherAUS.RData

This dataset is observations of weather metrics for each day from 2008 to 2017. The __weatherAUS.csv__ dataset includes following fields:

| Field          | Description                                             | Unit            | Type   |
|----------------|---------------------------------------------------------|-----------------|--------|
| Date           | Date of the Observation in YYYY-MM-DD                   | Date            | object |             
| Location       | Location of the Observation                             | Location        | object |
| MinTemp        | Minimum temperature                                     | Celsius         | float  |     
| MaxTemp        | Maximum temperature                                     | Celsius         | float  |
| Rainfall       | Amount of rainfall                                      | Millimeters     | float  |
| Evaporation    | Amount of evaporation                                   | Millimeters     | float  |
| Sunshine       | Amount of bright sunshine                               | hours           | float  |                  
| WindGustDir    | Direction of the strongest gust                         | Compass Points  | object |
| WindGustSpeed  | Speed of the strongest gust                             | Kilometers/Hour | object |
| WindDir9am     | Wind direction averaged of 10 minutes prior to 9am      | Compass Points  | object |
| WindDir3pm     | Wind direction averaged of 10 minutes prior to 3pm      | Compass Points  | object |
| WindSpeed9am   | Wind speed averaged of 10 minutes prior to 9am          | Kilometers/Hour | float  |
| WindSpeed3pm   | Wind speed averaged of 10 minutes prior to 3pm          | Kilometers/Hour | float  |
| Humidity9am    | Humidity at 9am                                         | Percent         | float  |
| Humidity3pm    | Humidity at 3pm                                         | Percent         | float  |
| Pressure9am    | Atmospheric pressure reduced to mean sea level at 9am   | Hectopascal     | float  |
| Pressure3pm    | Atmospheric pressure reduced to mean sea level at 3pm   | Hectopascal     | float  |
| Cloud9am       | Fraction of the sky obscured by cloud at 9am            | Eights          | float  |
| Cloud3pm       | Fraction of the sky obscured by cloud at 3pm            | Eights          | float  |
| Temp9am        | Temperature at 9am                                      | Celsius         | float  |
| Temp3pm        | Temperature at 3pm                                      | Celsius         | float  |
| RainToday      | If there was rain today                                 | Yes/No          | object |
| RISK_MM        | Amount of rain tomorrow                                 | Millimeters     | float  |
| RainTomorrow   | If there is rain tomorrow                               | Yes/No          | float  |


Column definitions were gathered from http://www.bom.gov.au/climate/dwo/IDCJDW0000.shtml

## Load the Dataset

Lets download the dataset


```python
!curl   https://s3.us.cloud-object-storage.appdomain.cloud/cf-courses-data/CognitiveClass/ML0101ENv3/project_EdX/weatherAUS.csv --output weatherAUS.csv
```

    --2020-02-11 16:15:09--  https://s3.us.cloud-object-storage.appdomain.cloud/cf-courses-data/CognitiveClass/ML0101ENv3/project_EdX/weatherAUS.csv
    Resolving s3.us.cloud-object-storage.appdomain.cloud (s3.us.cloud-object-storage.appdomain.cloud)... 67.228.254.196
    Connecting to s3.us.cloud-object-storage.appdomain.cloud (s3.us.cloud-object-storage.appdomain.cloud)|67.228.254.196|:443... connected.
    HTTP request sent, awaiting response... 200 OK
    Length: 14167326 (14M) [text/csv]
    Saving to: ‘weatherAUS.csv’
    
    weatherAUS.csv      100%[===================>]  13.51M  7.77MB/s    in 1.7s    
    
    2020-02-11 16:15:11 (7.77 MB/s) - ‘weatherAUS.csv’ saved [14167326/14167326]
    
    

Now we use the __head()__ function to see our data


```python
df = pd.read_csv('weatherAUS.csv')

df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Date</th>
      <th>Location</th>
      <th>MinTemp</th>
      <th>MaxTemp</th>
      <th>Rainfall</th>
      <th>Evaporation</th>
      <th>Sunshine</th>
      <th>WindGustDir</th>
      <th>WindGustSpeed</th>
      <th>WindDir9am</th>
      <th>...</th>
      <th>Humidity3pm</th>
      <th>Pressure9am</th>
      <th>Pressure3pm</th>
      <th>Cloud9am</th>
      <th>Cloud3pm</th>
      <th>Temp9am</th>
      <th>Temp3pm</th>
      <th>RainToday</th>
      <th>RISK_MM</th>
      <th>RainTomorrow</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2008-12-01</td>
      <td>Albury</td>
      <td>13.4</td>
      <td>22.9</td>
      <td>0.6</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>W</td>
      <td>44.0</td>
      <td>W</td>
      <td>...</td>
      <td>22.0</td>
      <td>1007.7</td>
      <td>1007.1</td>
      <td>8.0</td>
      <td>NaN</td>
      <td>16.9</td>
      <td>21.8</td>
      <td>No</td>
      <td>0.0</td>
      <td>No</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2008-12-02</td>
      <td>Albury</td>
      <td>7.4</td>
      <td>25.1</td>
      <td>0.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>WNW</td>
      <td>44.0</td>
      <td>NNW</td>
      <td>...</td>
      <td>25.0</td>
      <td>1010.6</td>
      <td>1007.8</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>17.2</td>
      <td>24.3</td>
      <td>No</td>
      <td>0.0</td>
      <td>No</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2008-12-03</td>
      <td>Albury</td>
      <td>12.9</td>
      <td>25.7</td>
      <td>0.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>WSW</td>
      <td>46.0</td>
      <td>W</td>
      <td>...</td>
      <td>30.0</td>
      <td>1007.6</td>
      <td>1008.7</td>
      <td>NaN</td>
      <td>2.0</td>
      <td>21.0</td>
      <td>23.2</td>
      <td>No</td>
      <td>0.0</td>
      <td>No</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2008-12-04</td>
      <td>Albury</td>
      <td>9.2</td>
      <td>28.0</td>
      <td>0.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NE</td>
      <td>24.0</td>
      <td>SE</td>
      <td>...</td>
      <td>16.0</td>
      <td>1017.6</td>
      <td>1012.8</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>18.1</td>
      <td>26.5</td>
      <td>No</td>
      <td>1.0</td>
      <td>No</td>
    </tr>
    <tr>
      <th>4</th>
      <td>2008-12-05</td>
      <td>Albury</td>
      <td>17.5</td>
      <td>32.3</td>
      <td>1.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>W</td>
      <td>41.0</td>
      <td>ENE</td>
      <td>...</td>
      <td>33.0</td>
      <td>1010.8</td>
      <td>1006.0</td>
      <td>7.0</td>
      <td>8.0</td>
      <td>17.8</td>
      <td>29.7</td>
      <td>No</td>
      <td>0.2</td>
      <td>No</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 24 columns</p>
</div>



## Preprocessing

We want to focus specifically on Sydney so that we can train our algorithm quickly. You can select other locations or multiple locations if you would like to experiment.

#### Drop Location & RIS_MM columns


```python
df_sydney = df[df['Location'] == 'Sydney']

df_sydney.drop(columns=['Location', 'RISK_MM'], axis=1, inplace=True)

print(df_sydney.shape)

df_sydney.head()
```

    (3337, 22)
    




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Date</th>
      <th>MinTemp</th>
      <th>MaxTemp</th>
      <th>Rainfall</th>
      <th>Evaporation</th>
      <th>Sunshine</th>
      <th>WindGustDir</th>
      <th>WindGustSpeed</th>
      <th>WindDir9am</th>
      <th>WindDir3pm</th>
      <th>...</th>
      <th>Humidity9am</th>
      <th>Humidity3pm</th>
      <th>Pressure9am</th>
      <th>Pressure3pm</th>
      <th>Cloud9am</th>
      <th>Cloud3pm</th>
      <th>Temp9am</th>
      <th>Temp3pm</th>
      <th>RainToday</th>
      <th>RainTomorrow</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>29497</th>
      <td>2008-02-01</td>
      <td>19.5</td>
      <td>22.4</td>
      <td>15.6</td>
      <td>6.2</td>
      <td>0.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>S</td>
      <td>SSW</td>
      <td>...</td>
      <td>92.0</td>
      <td>84.0</td>
      <td>1017.6</td>
      <td>1017.4</td>
      <td>8.0</td>
      <td>8.0</td>
      <td>20.7</td>
      <td>20.9</td>
      <td>Yes</td>
      <td>Yes</td>
    </tr>
    <tr>
      <th>29498</th>
      <td>2008-02-02</td>
      <td>19.5</td>
      <td>25.6</td>
      <td>6.0</td>
      <td>3.4</td>
      <td>2.7</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>W</td>
      <td>E</td>
      <td>...</td>
      <td>83.0</td>
      <td>73.0</td>
      <td>1017.9</td>
      <td>1016.4</td>
      <td>7.0</td>
      <td>7.0</td>
      <td>22.4</td>
      <td>24.8</td>
      <td>Yes</td>
      <td>Yes</td>
    </tr>
    <tr>
      <th>29499</th>
      <td>2008-02-03</td>
      <td>21.6</td>
      <td>24.5</td>
      <td>6.6</td>
      <td>2.4</td>
      <td>0.1</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>ESE</td>
      <td>ESE</td>
      <td>...</td>
      <td>88.0</td>
      <td>86.0</td>
      <td>1016.7</td>
      <td>1015.6</td>
      <td>7.0</td>
      <td>8.0</td>
      <td>23.5</td>
      <td>23.0</td>
      <td>Yes</td>
      <td>Yes</td>
    </tr>
    <tr>
      <th>29500</th>
      <td>2008-02-04</td>
      <td>20.2</td>
      <td>22.8</td>
      <td>18.8</td>
      <td>2.2</td>
      <td>0.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NNE</td>
      <td>E</td>
      <td>...</td>
      <td>83.0</td>
      <td>90.0</td>
      <td>1014.2</td>
      <td>1011.8</td>
      <td>8.0</td>
      <td>8.0</td>
      <td>21.4</td>
      <td>20.9</td>
      <td>Yes</td>
      <td>Yes</td>
    </tr>
    <tr>
      <th>29501</th>
      <td>2008-02-05</td>
      <td>19.7</td>
      <td>25.7</td>
      <td>77.4</td>
      <td>NaN</td>
      <td>0.0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NNE</td>
      <td>W</td>
      <td>...</td>
      <td>88.0</td>
      <td>74.0</td>
      <td>1008.3</td>
      <td>1004.8</td>
      <td>8.0</td>
      <td>8.0</td>
      <td>22.5</td>
      <td>25.5</td>
      <td>Yes</td>
      <td>Yes</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 22 columns</p>
</div>



As you can see above we have NaN occur a couple times in our dataset. We can either drop the data or replace the data.


```python
df_sydney.isna().sum()
```




    Date                0
    MinTemp             3
    MaxTemp             2
    Rainfall            6
    Evaporation        51
    Sunshine           16
    WindGustDir      1036
    WindGustSpeed    1036
    WindDir9am         56
    WindDir3pm         33
    WindSpeed9am       26
    WindSpeed3pm       25
    Humidity9am        14
    Humidity3pm        13
    Pressure9am        20
    Pressure3pm        19
    Cloud9am          566
    Cloud3pm          561
    Temp9am             4
    Temp3pm             4
    RainToday           6
    RainTomorrow        0
    dtype: int64




```python
df_sydney.Evaporation.replace(np.NaN,df_sydney.Evaporation.median(),inplace=True)
df_sydney.WindGustSpeed.replace(np.NaN,df_sydney.WindGustSpeed.median(),inplace=True)
df_sydney.WindSpeed9am.replace(np.NaN,df_sydney.WindSpeed9am.median(),inplace=True)
df_sydney.WindSpeed3pm.replace(np.NaN,df_sydney.WindSpeed3pm.median(),inplace=True)
df_sydney.Cloud9am.replace(np.NaN,df_sydney.Cloud9am.median(),inplace=True)
df_sydney.Cloud3pm.replace(np.NaN,df_sydney.Cloud3pm.median(),inplace=True)
df_sydney.WindGustDir.mode()
df_sydney.WindGustDir.replace(np.NaN,'W',inplace=True)
df_sydney.WindDir9am.mode()
df_sydney.WindDir9am.replace(np.NaN,'W',inplace=True)
df_sydney.WindDir3pm.mode()
df_sydney.WindDir3pm.replace(np.NaN,'E',inplace=True)
df_sydney.dropna(inplace=True)
```


```python
print(df_sydney.shape)
df_sydney.isna().sum()
```

    (1690, 22)
    




    Date             0
    MinTemp          0
    MaxTemp          0
    Rainfall         0
    Evaporation      0
    Sunshine         0
    WindGustDir      0
    WindGustSpeed    0
    WindDir9am       0
    WindDir3pm       0
    WindSpeed9am     0
    WindSpeed3pm     0
    Humidity9am      0
    Humidity3pm      0
    Pressure9am      0
    Pressure3pm      0
    Cloud9am         0
    Cloud3pm         0
    Temp9am          0
    Temp3pm          0
    RainToday        0
    RainTomorrow     0
    dtype: int64



As you can see we have completely removed all NaN values using different methods which allowed used to either remove remove rows with NaN in them improving the pureness of our dataset or filling in NaN values allowing us to preserve rows. When deciding on the method to use there are many benefits and drawbacks we must consider like whether or not we will have enough data after dropping NaN rows or if filling in Nan by frequency or mean will introduce some sort of bias to our data.


```python
df_sydney_filled.loc[:,'Date'] = df.Date.str.replace('-', '')
```

    /home/jupyterlab/conda/envs/python/lib/python3.6/site-packages/pandas/core/indexing.py:494: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: http://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      self.obj[item] = s
    

Finally we remove the - between the values of the Date column so they can be converted to floats

## One Hot Encoding

Finally we need to perform one hot encoding to convert categorical variables to binary variables


```python
df_sydney_processed = pd.get_dummies(data=df_sydney, columns=['RainToday', 'WindGustDir', 'WindDir9am', 'WindDir3pm'])
```

Next we replace the values of the RainTomorrow column changing it from a categorical column to a binary column. We do not use the __get_dummies__ method because we would end up with two columns for RainTomorrow and we do not want that because it is our target.


```python
df_sydney_processed.replace(['No', 'Yes'], [0,1], inplace=True)
```

## Training Data and Testing Data

First we turn all columns into a float type. We don't need to do this because the __StandardScalar()__ method will convert object types to float but it will give us a warning message.


```python
df_sydney_processed = df_sydney_processed.astype(float)
```

Now we split our dataset into a features dataset and target dataset. We drop our target to create our features dataset and only keep RainTomorrow to create our target dataset


```python
features = df_sydney_processed.drop(columns='RainTomorrow', axis=1)
Y = df_sydney_processed['RainTomorrow']
```

Before we Normalize our data we must split it into training and testing sets. We do this before Normalization so that we don't give any hints to out model by Normalizing all the data together.


```python
x_train, x_test, y_train, y_test = train_test_split(features, Y, test_size=.2, random_state=1)
```

Please uncomment the method you would like to choose


```python
norm = preprocessing.StandardScaler()
```


```python
x_train = norm.fit_transform(x_train)

x_test = norm.transform(x_test)
```

As we discussed before you can see how we fit and the scaler to the training data and also transformed it. Then we used the fitted scaler to transform the test data.

# Classification

## Instructions

Below is where we are going to use the classification algorithms to create a model based on our training data and finally evaluate our testing data using evaluation metrics learned in the course

We will some of the algorithms taught in the course, specifically 

1. Logistic Regression 
2. KNN
3. SVM
4. Decision Trees

We will evaluate our models using

1. Accuracy Score
2. Jaccard Index
3. F1-Score
4. Log Loss

Note: Jaccard Index is calculated differently in Sci Kit Learn so I have defined a function at the top of the notebook for you to use, its input style is the same as Sci Kit Learn

As we know these algorithms have many parameters and to find the best ones we will use GridSearchCV


You will need to research the parameters you need to use as there are many options but this is simple. GridSearchCV will determine the best model.

Finally using your models generate the report at the bottom

## Logistic Regression

When creating the LogisticRegression object please make max_iter = 1000. This will allow us enough iteration so the model parameters can converge


```python
%%time

parameters = {'C': [.001, .01, .1, 1, 10, 100], 
              'solver': ['newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga'] }

LR = LogisticRegression(max_iter=1000)

Grid = GridSearchCV(LR, parameters,cv=4)

Grid.fit(x_train, y_train)
```

    CPU times: user 45.6 s, sys: 44.7 s, total: 1min 30s
    Wall time: 34.4 s
    




    GridSearchCV(cv=4, error_score='raise-deprecating',
           estimator=LogisticRegression(C=1.0, class_weight=None, dual=False, fit_intercept=True,
              intercept_scaling=1, max_iter=1000, multi_class='warn',
              n_jobs=None, penalty='l2', random_state=None, solver='warn',
              tol=0.0001, verbose=0, warm_start=False),
           fit_params=None, iid='warn', n_jobs=None,
           param_grid={'C': [0.001, 0.01, 0.1, 1, 10, 100], 'solver': ['newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga']},
           pre_dispatch='2*n_jobs', refit=True, return_train_score='warn',
           scoring=None, verbose=0)




```python
BestLR = Grid.best_estimator_
```


```python
predictions = BestLR.predict(x_test)

LR_Accuracy_Score = accuracy_score(predictions, y_test)
LR_JaccardIndex = jaccard_index(predictions, y_test)
LR_F1_Score = f1_score(predictions, y_test)
LR_Log_Loss = log_loss(y_test,BestLR.predict_proba(x_test)[:, 1])
```

## KNN


```python
%%time

parameters = {'n_neighbors': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 'algorithm': ['auto', 'ball_tree', 'kd_tree', 'brute'], 'p': [1,2]}

KNN = KNeighborsClassifier()

Grid = GridSearchCV(KNN, parameters,cv=4)

Grid.fit(x_train, y_train)
```

    CPU times: user 1min 26s, sys: 22.8 s, total: 1min 48s
    Wall time: 1min 21s
    




    GridSearchCV(cv=4, error_score='raise-deprecating',
           estimator=KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski',
               metric_params=None, n_jobs=None, n_neighbors=5, p=2,
               weights='uniform'),
           fit_params=None, iid='warn', n_jobs=None,
           param_grid={'n_neighbors': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 'algorithm': ['auto', 'ball_tree', 'kd_tree', 'brute'], 'p': [1, 2]},
           pre_dispatch='2*n_jobs', refit=True, return_train_score='warn',
           scoring=None, verbose=0)




```python
BestKNN = Grid.best_estimator_
```


```python
predictions = BestKNN.predict(x_test)

KNN_Accuracy_Score = accuracy_score(predictions, y_test)
KNN_JaccardIndex = jaccard_index(predictions, y_test)
KNN_F1_Score = f1_score(predictions, y_test)
```

## SVM


```python
%%time

parameters = {'C': [.001, .01, .1, 1, 10, 100], 'kernel': ['linear', 'poly', 'rbf', 'sigmoid']}

SVM = svm.SVC(gamma = 'auto')

Grid = GridSearchCV(SVM, parameters,cv=4)

Grid.fit(x_train, y_train)
```

    CPU times: user 2min 8s, sys: 0 ns, total: 2min 8s
    Wall time: 2min 10s
    




    GridSearchCV(cv=4, error_score='raise-deprecating',
           estimator=SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
      decision_function_shape='ovr', degree=3, gamma='auto', kernel='rbf',
      max_iter=-1, probability=False, random_state=None, shrinking=True,
      tol=0.001, verbose=False),
           fit_params=None, iid='warn', n_jobs=None,
           param_grid={'C': [0.001, 0.01, 0.1, 1, 10, 100], 'kernel': ['linear', 'poly', 'rbf', 'sigmoid']},
           pre_dispatch='2*n_jobs', refit=True, return_train_score='warn',
           scoring=None, verbose=0)




```python
BestSVM = Grid.best_estimator_
```


```python
predictions = BestSVM.predict(x_test)

SVM_Accuracy_Score = accuracy_score(predictions, y_test)
SVM_JaccardIndex = jaccard_index(predictions, y_test)
SVM_F1_Score = f1_score(predictions, y_test)
```

## Decision Tree


```python
%%time

parameters = {'criterion':['gini', 'entropy']}

Tree = DecisionTreeClassifier()

Grid = GridSearchCV(Tree, parameters,cv=4)

Grid.fit(x_train, y_train)
```

    CPU times: user 234 ms, sys: 0 ns, total: 234 ms
    Wall time: 243 ms
    




    GridSearchCV(cv=4, error_score='raise-deprecating',
           estimator=DecisionTreeClassifier(class_weight=None, criterion='gini', max_depth=None,
                max_features=None, max_leaf_nodes=None,
                min_impurity_decrease=0.0, min_impurity_split=None,
                min_samples_leaf=1, min_samples_split=2,
                min_weight_fraction_leaf=0.0, presort=False, random_state=None,
                splitter='best'),
           fit_params=None, iid='warn', n_jobs=None,
           param_grid={'criterion': ['gini', 'entropy']},
           pre_dispatch='2*n_jobs', refit=True, return_train_score='warn',
           scoring=None, verbose=0)




```python
BestTree = Grid.best_estimator_
```


```python
predictions = BestTree.predict(x_test)

Tree_Accuracy_Score = accuracy_score(predictions, y_test)
Tree_JaccardIndex = jaccard_index(predictions, y_test)
Tree_F1_Score = f1_score(predictions, y_test)
```

# Report


```python
Report = pd.DataFrame({'Algorithm' : ['KNN', 'Decision Tree', 'SVM', 'LogisticRegression']})

Report['Accuracy'] = [LR_Accuracy_Score, KNN_Accuracy_Score, SVM_Accuracy_Score, Tree_Accuracy_Score]
Report['Jaccard'] = [LR_JaccardIndex, KNN_JaccardIndex, SVM_JaccardIndex, Tree_JaccardIndex]
Report['F1-Score'] = [LR_F1_Score, KNN_F1_Score, SVM_F1_Score, Tree_F1_Score]
Report['LogLoss'] = ['N/A', 'N/A', 'N/A', LR_Log_Loss]

Report
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Algorithm</th>
      <th>Accuracy</th>
      <th>Jaccard</th>
      <th>F1-Score</th>
      <th>LogLoss</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>KNN</td>
      <td>0.852071</td>
      <td>0.742268</td>
      <td>0.691358</td>
      <td>N/A</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Decision Tree</td>
      <td>0.816568</td>
      <td>0.690000</td>
      <td>0.530303</td>
      <td>N/A</td>
    </tr>
    <tr>
      <th>2</th>
      <td>SVM</td>
      <td>0.852071</td>
      <td>0.742268</td>
      <td>0.666667</td>
      <td>N/A</td>
    </tr>
    <tr>
      <th>3</th>
      <td>LogisticRegression</td>
      <td>0.792899</td>
      <td>0.656863</td>
      <td>0.611111</td>
      <td>0.344221</td>
    </tr>
  </tbody>
</table>
</div>


