<a href="https://cognitiveclass.ai/">
    <img src="https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/PY0101EN/Ad/CCLog.png"width="200" align="center">
</a>
<h1>Bank Credit Card Data Predicting Annual Income   </h1>
<h2>Description</h2>



### A Credit Card Dataset for Machine Learning!

Credit cards are a common risk control method in the financial industry. It uses personal information and data submitted by credit card applicants to predict credit card borrowings. The bank is able to decide whether to issue a credit card to the applicant. 
 
At present, with the development of machine learning algorithms. However, these methods often do not have good transparency. It may be difficult to provide customers and regulators with a reason for rejection or acceptance.

Banks objective is to predict the threshold annual income so that they can set some standards for  the process of accepting and rejecting the applications for the product! 

<h2>Table of Contents</h2>
<div class="alert alert-block alert-info" style="margin-top: 20px">
    <ul>
        <li><a href="#Section_1"> Importing Data </a></li>
    <li><a href="#Section_2">Question 1: Display the data types of each column using the attribute dtype </a> </li>
    <li><a href="#Section_3">Question 2:  Use the method value_counts to count the frequency of Name Income Type, use the method .to_frame () to convert it to a dataframe. </a></li>
    <li><a href="#Section_4">Question 3: Perform a statistical summary and analysis of Years_Working using describe() function </a></li>
    <li><a href="#Section_5">Question 4:Use the function boxplot in the seaborn library to produce a plot that can be used to show the Years_Working. </a></li>
    <li><a href="#Section_6">Question 5 Use the function regplot in the seaborn library to determine if the Age is negatively or positively correlated with the Annual Income. </a></li>
    <li><a href="#Section_7">Question 6 Fit a linear regression model to predict the Years_Working against the annual income and then calculate R^2. Take a screenshot of your code and the value of the R^2.  </a></li>
    <li><a href="#Section_8">Question 7 Make a distribution plot of actual and fitted values. Interpret the results.  </a></li>
    <li><a href="#Section_9">Question 8 Create a pipeline object that scales the data performs a polynomial transform and fits a linear regression model. Fit the object using the features in the question above, then fit the model and calculate the R^2. Take a screenshot of your code and the R^2.</a></li>
    <li><a href="#Section_10">Question 9 Identify optimal regularization parameter value using grid search. Then create and fit a Ridge regression object using the training data, setting the regularization parameter to 0.1 and calculate the R^2 using the test data. Take a screenshot for your code and the R^2 </a></li>
<li><a href="#Section_11">Question 10 Perform a second order polynomial transform on both the training data and testing data. Create and fit a Ridge regression object using the training data, identifying the regularization parameter value using grid search </a></li>
<p>Estimated Time Needed: <strong>180 min</strong></p>
</div>

<hr>

### Important Libraries


```python
!pip install seaborn

```


```python
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression as LR
from sklearn.metrics import r2_score
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import Pipeline
from sklearn.linear_model import Ridge
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split as tts
```

### Importing Data


```python
!curl https://raw.githubusercontent.com/hskillup/Course_6_Data_Analysis/master/creditcard_data.csv --output creditcard_data.csv
```

      % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                     Dload  Upload   Total   Spent    Left  Speed
    
      0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
      0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
      3  340k    3 12339    0     0   8665      0  0:00:40  0:00:01  0:00:39  8665
     18  340k   18 65808    0     0  31292      0  0:00:11  0:00:02  0:00:09 31292
     78  340k   78  266k    0     0  87924      0  0:00:03  0:00:03 --:--:-- 87924
    100  340k  100  340k    0     0   103k      0  0:00:03  0:00:03 --:--:--  103k
    


```python
Data=pd.read_csv('creditcard_data.csv')
```

### Question 1 : Display the data types of each column using the attribute dtype 


```python
# Post Your Answer Here and Execute

```

### Question 2 : Use the method value_counts to count the frequency of Name Income Type, use the method .to_frame () to convert it to a dataframe. 


```python
# Post Your Answer Here and Execute

```

### Question 3 Perform a statistical summary and analysis of Years_Working using describe() function.  



```python
# Post Your Answer Here and Execute

```

### Question 4: Use the function boxplot in the seaborn library to produce a plot that can be used to show the Years_Working. 



```python
# Post Your Answer Here and Execute

```

### Removing Outliers


```python
sns.boxplot(Data['AMT_INCOME_TOTAL'])
```




    <matplotlib.axes._subplots.AxesSubplot at 0x256c5518d48>




![png](Course-6-MD/images/output_19_1.png)


### User Defined function to remove outliers 


```python
def outlier(col):
    IQR=Data[col].quantile(.75)-Data[col].quantile(.25)
    upper_bound=Data[col].quantile(.75) + ( 1.5 * IQR)
    lower_bound=Data[col].quantile(.25) - ( 1.5 * IQR)
    Data[col].clip(lower=lower_bound,upper=upper_bound,inplace=True)
```


```python
outlier('AMT_INCOME_TOTAL')
```


```python
outlier('Years_Working')
```

### Using one-hot encoding on  Categorical Variables


```python
Categ_Data=pd.get_dummies(Data.select_dtypes('object'),drop_first=True) 
```


```python
Data.drop(Data.select_dtypes('object').columns.values.tolist(),axis=1,inplace=True)
```


```python
Data=pd.concat([Data,Categ_Data],axis=1)
```


```python
Data.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ID</th>
      <th>CNT_CHILDREN</th>
      <th>AMT_INCOME_TOTAL</th>
      <th>FLAG_MOBIL</th>
      <th>FLAG_WORK_PHONE</th>
      <th>FLAG_PHONE</th>
      <th>FLAG_EMAIL</th>
      <th>CNT_FAM_MEMBERS</th>
      <th>MONTHS_BALANCE</th>
      <th>Age</th>
      <th>...</th>
      <th>NAME_HOUSING_TYPE_House / apartment</th>
      <th>NAME_HOUSING_TYPE_Municipal apartment</th>
      <th>NAME_HOUSING_TYPE_Office apartment</th>
      <th>NAME_HOUSING_TYPE_Rented apartment</th>
      <th>NAME_HOUSING_TYPE_With parents</th>
      <th>STATUS_1</th>
      <th>STATUS_2</th>
      <th>STATUS_5</th>
      <th>STATUS_C</th>
      <th>STATUS_X</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>5008838</td>
      <td>1</td>
      <td>129820.0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>3</td>
      <td>-9</td>
      <td>32</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>5008839</td>
      <td>1</td>
      <td>129820.0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>3</td>
      <td>0</td>
      <td>32</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>5008840</td>
      <td>1</td>
      <td>129820.0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>3</td>
      <td>0</td>
      <td>32</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5008841</td>
      <td>1</td>
      <td>129820.0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>3</td>
      <td>0</td>
      <td>32</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5008842</td>
      <td>1</td>
      <td>129820.0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>3</td>
      <td>0</td>
      <td>32</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 35 columns</p>
</div>



### Question 5 Use the function regplot in the seaborn library to determine if the Age is negatively or positively correlated with the Annual Income. 



```python
# Post Your Answer Here and Execute

```

### Question 6 Fit a linear regression model to predict the Years_Working against the annual income and then calculate R^2. Take a screenshot of your code and the value of the R^2. 


```python
X=np.asarray(Data.Years_Working).reshape(-1,1)
y=np.asarray(Data.AMT_INCOME_TOTAL).reshape(-1,1)
```


```python
# Post Your Answer Here and Execute

```


### Question 7 Make a distribution plot of actual and fitted values. Interpret the results.


```python
# Post Your Answer Here and Execute

```

### Question 8 Create a pipeline object that scales the data performs a polynomial transform and fits a linear regression model. Fit the object using the features in the question above, then fit the model and calculate the R^2. Take a screenshot of your code and the R^2. 


```python
X_cols=['MONTHS_BALANCE','Age','Years_Working','NAME_FAMILY_STATUS_Married','NAME_HOUSING_TYPE_Rented apartment']#Select Independent Variables
y_col=['AMT_INCOME_TOTAL']#Select Dependent Variable
```


```python
X_Data=Data[X_cols]
y=Data[y_col]
```


```python
# Post Your Answer Here and Execute

```


```python
sns.distplot(y.AMT_INCOME_TOTAL,hist=False,label="Actual")
sns.distplot(ypipe,hist=False,label='Predicted')
```




    <matplotlib.axes._subplots.AxesSubplot at 0x256d32e8808>




![png](Course-6-MD/images/output_40_1.png)


### Question 9 Identify optimal regularization parameter value using grid search. Then create and fit a Ridge regression object using the training data, setting the regularization parameter to 0.1 and calculate the R^2 using the test data. Take a screenshot for your code and the R^2 


```python
# Post Your Answer Here and Execute

```

### Question 10 Perform a second order polynomial transform on both the training data and testing data. Create and fit a Ridge regression object using the training data, identifying the regularization parameter value using grid search

### Splitting Data


```python
train_X,test_X,train_y,test_Y=tts(X_Data,y,test_size=.10,random_state=20) 
```


```python
# Post Your Answer Here and Execute

```

<h2 id="Section_5">  How to submit </h2>

#### **Make sure to toggle on 'Sahre with anyone who has the link' as shown below**

<p>Once you complete your notebook you will have to share it to be marked. Select the icon on the top right a marked in red in the image below, a dialogue box should open, select the option all&nbsp;content excluding sensitive code cells.</p>

<p><img height="440" width="700" src="https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/PY0101EN/projects/EdX/ReadMe%20files/share_noteook1.png" alt="share notebook" /></p>
<p></p>

<p>You can then share the notebook&nbsp; via a&nbsp; URL by scrolling down as shown in the following image:</p>
<p style="text-align: center;"> <img height="308" width="350" src="https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/PY0101EN/projects/EdX/ReadMe%20files/link2.png"  alt="share notebook" /> </p>


<h2>About the Authors:</h2> 

<a href="https://www.linkedin.com/in/joseph-s-50398b136/">Joseph Santarcangelo</a> has a PhD in Electrical Engineering, his research focused on using machine learning, signal processing, and computer vision to determine how videos impact human cognition. Joseph has been working for IBM since he completed his PhD.


### References :

 <a href="https://www.kaggle.com/rikdifos/credit-card-approval-prediction/data?select=credit_record.csv">Bank Data Set </a>
    
    



```python

```
