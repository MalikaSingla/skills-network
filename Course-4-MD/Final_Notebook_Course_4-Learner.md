<a href="https://cognitiveclass.ai/">
    <img src="https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/PY0101EN/Ad/CCLog.png" width="200" align="center">
</a>
<h1>Analyzing US Hourly Climate Data and  Building a Dashboard  </h1>
<h2>Description</h2>



Extracting essential data from a dataset and displaying it is a necessary part of data science; therefore individuals can make correct decisions based on the data. In this assignment, you will extract some essential climate indicators from some data, you will then display these indicators in a Dashboard. You can then share the dashboard via an URL.


   - **STATION** (17 characters) is the station identification code
    
   - **STATION_NAME**  is the name of the station (usually city/airport name).
    
   - **Date** is the date and time of recording the climate data
        
   - **Latitude** & **Latitude** are the Geo-Location of the place
        
   - **Temperature** Temperature recorded in Fahrenheit
        
   - **WindSpeed** speed of wind recorded
        
   - **HourlyWindDirection** Direction of wind 

<h2>Table of Contents</h2>
<div class="alert alert-block alert-info" style="margin-top: 20px">
    <ul>
        <li><a href="#Section_1"> Define a Function that Makes a Dashboard </a></li>
    <li><a href="#Section_2">Question 1: Create a dataframe that contains climate data and display using the method head(). Take a screenshot.</a> </li>
    <li><a href="#Section_3">Question 2: Get the Mininum and Maximum temperatures. Display the first five rows of the dataframe. Take a screenshot</a></li>
    <li><a href="#Section_4">Question 3: Create a dataframe where HourlyStationPressure was greater than 29.35mm. Take a screenshot.</a></li>
    <li><a href="#Section_5">Question 4: 4- Use the function make_dashboard to make a dashboard using Temperature, Humidity. Take a screenshot</a></li>
        
   
<p>
    Estimated Time Needed: <strong>180 min</strong></p>
</div>

<hr>

## Importing csv file from GitHub


```python
!curl    https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-PY0101EN-SkillUp/labs/Climate_Data.csv --output us_climate.csv
```

      % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                     Dload  Upload   Total   Spent    Left  Speed
    
      0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
    100    98    0    98    0     0    191      0 --:--:-- --:--:-- --:--:--   191
    

## Importing Libraries


```python
!pip install bokeh
```

    Requirement already satisfied: bokeh in c:\users\skillup 08\desktop\proj_env\lib\site-packages (2.1.0)
    Requirement already satisfied: PyYAML>=3.10 in c:\users\skillup 08\desktop\proj_env\lib\site-packages (from bokeh) (5.3.1)
    Requirement already satisfied: Jinja2>=2.7 in c:\users\skillup 08\desktop\proj_env\lib\site-packages (from bokeh) (2.11.2)
    Requirement already satisfied: tornado>=5.1 in c:\users\skillup 08\desktop\proj_env\lib\site-packages (from bokeh) (6.0.4)
    Requirement already satisfied: numpy>=1.11.3 in c:\users\skillup 08\desktop\proj_env\lib\site-packages (from bokeh) (1.18.5)
    Requirement already satisfied: typing-extensions>=3.7.4 in c:\users\skillup 08\desktop\proj_env\lib\site-packages (from bokeh) (3.7.4.2)
    Requirement already satisfied: pillow>=4.0 in c:\users\skillup 08\desktop\proj_env\lib\site-packages (from bokeh) (7.1.2)
    Requirement already satisfied: packaging>=16.8 in c:\users\skillup 08\desktop\proj_env\lib\site-packages (from bokeh) (20.4)
    Requirement already satisfied: python-dateutil>=2.1 in c:\users\skillup 08\desktop\proj_env\lib\site-packages (from bokeh) (2.8.1)
    Requirement already satisfied: MarkupSafe>=0.23 in c:\users\skillup 08\desktop\proj_env\lib\site-packages (from Jinja2>=2.7->bokeh) (1.1.1)
    Requirement already satisfied: six in c:\users\skillup 08\desktop\proj_env\lib\site-packages (from packaging>=16.8->bokeh) (1.14.0)
    Requirement already satisfied: pyparsing>=2.0.2 in c:\users\skillup 08\desktop\proj_env\lib\site-packages (from packaging>=16.8->bokeh) (2.4.7)
    

    WARNING: You are using pip version 20.0.2; however, version 20.1.1 is available.
    You should consider upgrading via the 'c:\users\skillup 08\desktop\proj_env\scripts\python.exe -m pip install --upgrade pip' command.
    


```python
import pandas as pd
import numpy as np
from bokeh.io import push_notebook, output_file,show, output_notebook

from bokeh.layouts import row

from bokeh.plotting import figure
import matplotlib.pyplot as plt
```


```python
output_notebook()
```



<div class="bk-root">
    <a href="https://bokeh.org" target="_blank" class="bk-logo bk-logo-small bk-logo-notebook"></a>
    <span id="1001">Loading BokehJS ...</span>
</div>




## Reading CSV in dataframe


```python
df=pd.read_csv('us_climate.csv')
```


```python
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>&lt;html&gt;&lt;body&gt;You are being &lt;a href="https://gitlab.com/users/sign_in"&gt;redirected&lt;/a&gt;.&lt;/body&gt;&lt;/html&gt;</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
</div>



## Question 1-  Create a dataframe that contains climate data and display using the method head(). Take a screenshot


```python

```

## Question 2- Using the OR condition, display all the rows with Minimum and Maximum temperature recorded. Take a screenshot. 


```python

```

## Question 3- Using value_counts() function, create a dataframe that displays the top five visibility readings. Take a screenshot.


```python

```

## Question 4- Use the function make_dashboard to make a dashboard using Temperature, Humidity variable. Take a screenshot

In this section, you will call the function make_dashboard , to produce a dashboard. We will use the convention of giving each variable the same name as the function parameter.

Assign Humidity column of your dataframe to variable Humidity and Temperature column of your dataframe to variable Temp


```python
def make_dashboard( Humidity, Temp,title, file_name):
    output_file(file_name,root_dir='inline',mode='relative')
    p = figure(title=title, x_axis_label='Humidity', y_axis_label='Temperature')
    p.scatter(Humidity.squeeze(), Temp.squeeze())
    #p.line(Humidity.squeeze(),WindSpeed.squeeze(), line_width=4, legend="Visibility")
    show(p)
```

Assign Humidity column of your data frame to **Humidity variable**


```python
Humidity = # Assign Humidity Column
```

Assign Temperature column of your data frame to **Temperature variable**


```python
Temperature= #Assign Temperature Column
```

Give your dashboard a title of string data type 


```python
title= #Add Title
```

Give file_name to your dashboard as "index.html"


```python

file_name =  #Give filename as index.html
```

Run the make_dashboard function using all the parameters defined above


```python
# Fill up the parameters in the following function:
# make_dashboard(Humidity=,Temp=,title=,file_name=)

```

<h2 id="Section_5">  How to submit </h2>

#### **Make sure to toggle on 'Share with anyone who has the link' as shown below**

<p>Once you complete your notebook you will have to share it to be marked. Select the icon on the top right a marked in red in the image below, a dialogue box should open, select the option all&nbsp;content excluding sensitive code cells.</p>

<p><img height="440" width="700" src="https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/PY0101EN/projects/EdX/ReadMe%20files/share_noteook1.png" alt="share notebook" /></p>
<p></p>

<p>You can then share the notebook&nbsp; via a&nbsp; URL by scrolling down as shown in the following image:</p>
<p style="text-align: center;"> <img height="308" width="350" src="https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/PY0101EN/projects/EdX/ReadMe%20files/link2.png"  alt="share notebook" /> </p>



```python

```

<h2>About the Authors:</h2> 

<a href="https://www.linkedin.com/in/joseph-s-50398b136/">Joseph Santarcangelo</a> has a PhD in Electrical Engineering, his research focused on using machine learning, signal processing, and computer vision to determine how videos impact human cognition. Joseph has been working for IBM since he completed his PhD.
<p>

</p>

<h2>References :</h2> 

 <a href="https://www.ncdc.noaa.gov/cdo-web/datasets">Climate Data </a>
    
    



```python

```